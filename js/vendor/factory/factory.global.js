$(function(){
    if($('.fancybox').length > 0)
        $('.fancybox').fancybox();

    $('.ajax-form-send, [action="/ajax/"]').each(function(){
    	var $form = $(this),
    		error = new function(){
    			var me = this,
    				inited = false,
		    		$elements = $form.find('[name]'),
    				CSS_CLASS = 'ajax-form-error';

    			$.extend(this, {
    				show : function(text){
    					if(!text){
    						return me;
    					}
		    			alert('Ошибка: ' + text);
		    			return me;
    				},
    				highlight : function(names){
    					if(!names || !names.length){
    						return me;
    					}
    					me.init();
	    				$.each(names, function(i, name){
	    					$elements.filter('[name="' + name + '"]').addClass(CSS_CLASS);
	    				});
	    				return me;
    				},
    				init : function(){
    					if(inited){
    						return me;
    					}

    					inited = true;

    					function callback(){
			    			$(this).removeClass(CSS_CLASS);
			    		}

    					$elements
    						.keyup(callback)
    						.change(callback);

    					return me;
    				}
    			});
    		};

    	$form.ajaxForm({
	    	success : function(response){
	    		console.log(response);
	    		if(response.error){
	    			error
	    				.show(response.info)
	    				.highlight(response.names);
	    			return;
	    		}

	    		if(response.info){
	    			alert(response.info);
	    		}
	    	}
	    });


    });
});


function plural(n, forms) {
    return forms[n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2];
}